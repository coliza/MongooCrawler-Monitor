const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
var APP_PATH = path.resolve(__dirname, 'templates');
module.exports = {
    mode: "development",
    //页面入口文件配置    
    entry: {
        index: './index.js'
    },
    externals: {
        'react': 'React'
    },
    devtool: 'eval-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: "initial",
                    minChunks: 2,
                    maxInitialRequests: 5,
                    minSize: 0
                },
                vendor: {
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    priority: 10,
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "less-loader"
                ]
            },
            {
                include: [path.resolve(__dirname, './'),],
                exclude: /(node_modules|bower_components)/,
                test: /\.js|\.jsx$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                'babel-preset-stage-1',
                                'babel-preset-es2015',
                                'babel-preset-react'
                            ].map(require.resolve)
                        }
                    }],

            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({template: './templates/index.html'})
    ],
    //入口文件输出配置 
    output: {
        path: path.join(__dirname, "static/js/app"),
        filename: "app.js",
    }
};