let React = require('react');
let ReactDOM = require('react-dom');

class App extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                React+Webpack+ES6从环境搭建到HelloWorld
            </div>
        );
    }
}

document.getElementById("app").innerHTML = "hello webpack";
ReactDOM.render(<App/>, document.getElementById('app'));