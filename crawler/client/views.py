import json

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render

from crawler.common.http import HttpUtils
from crawler.site.models import ClientInfo

def clients(request):
    return render(request, 'client/client.html')
def clients_info(request):
    url = settings.CRAWLER_BASE_URL
    url += "config/clients"
    data = HttpUtils.get(url)
    # 封装服务器配置对象
    clients_info = json.dumps(ClientInfo.parse(data))
    return HttpResponse(clients_info, content_type="application/json")
