"""mongoo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.views import static

from crawler.business.views import businesses, lianjia, map, houses_json, lianjia_crawl
from crawler.client.views import clients, clients_info
from crawler.server.views import config_json
from crawler.site.views import index

urlpatterns = [
    url(r'^$', index),
    url(r'^config/server', config_json),
    url(r'^clients$', clients),
    url(r'^config/clients$', clients_info),
    url(r'^business$', businesses),
    url(r'^business/lianjia$', lianjia),
    url(r'^business/lianjia/crawl$', lianjia_crawl),
    url(r'^business/lianjia_json$', houses_json),
    url(r'^business/map', map),
    url(r'^static/(?P<path>.*)$', static.serve, {'document_root': settings.DOCUMENT_ROOT}, name="static"),
]
