import json

from django.conf import settings
from django.http import HttpResponse
from django.template import RequestContext

from crawler.common.http import HttpUtils
from crawler.site.models import ServerConfig


def config_json(request):
    url = settings.CRAWLER_BASE_URL
    url += "config/server"
    data = HttpUtils.get(url)
    # 封装服务器配置对象
    server_config = json.dumps(ServerConfig.parse(data))
    return HttpResponse(server_config, content_type="application/json")
