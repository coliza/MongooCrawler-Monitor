import requests


class HttpUtils:

    @staticmethod
    def get(url, params={}):
        resp = requests.get(url, params)
        status_code = resp.status_code
        if status_code == 200:
            return resp.json()
        raise Exception()