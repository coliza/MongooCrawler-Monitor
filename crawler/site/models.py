class ServerConfig:
    def ServerConfig(self):
        self.mode = ""
        self.run_status = "已关闭"
        self.crawler_number = 0

    @staticmethod
    def parse(json):
        server_config = {}
        server_config["mode"] = json["modeLabel"]
        server_config["run_status"] = json["runStatus"]
        server_config["crawler_number"] = json["crawlerCount"]
        return server_config


class ClientInfo:
    def ClientInfo(self):
        self.ip = ""
        self.dest_url = ""
        self.last_connect_time = ""
        self.cur_connect_time = ""

    @staticmethod
    def parse(json):
        clients = []
        for obj in json:
            client_info = {}
            client_info["ip"] = obj["ip"]
            client_info["status"] = obj["statusEnum"]
            client_info["last_connect_time"] = obj["connectTime"]
            client_info["cur_connect_time"] = obj["connectTime"]
            clients.append(client_info)
        return clients
