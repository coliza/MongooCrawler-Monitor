from django.db import models


class House(models.Model):
    def House(self):
        self.name = models.CharField(max_length=255)
        self.address = models.TextField(null=True)
        self.area = models.IntegerField(max_length=11, default=0)
        self.amount = models.IntegerField(max_length=11, default=0)
        self.price = models.FloatField(max_length=11, default=0)
